package presentation

import (
	"fmt"
	"home/aidamur/dev/selenium/internal/domain"
)

type VacancyPresenter struct{}

func NewVacancyPresenter() *VacancyPresenter {
	return &VacancyPresenter{}
}

func (p *VacancyPresenter) PresentVacancies(vacancies []domain.Vacancy) {
	for _, vacancy := range vacancies {
		fullLink := "https://career.habr.com" + vacancy.Link
		fmt.Printf("Title: %s\nLink: %s\n\n", vacancy.Title, fullLink)
	}
}

func (p *VacancyPresenter) PresentVacancyCount(count string) {
	fmt.Printf("Vacancy Count: %v\n\n", count)
}

func (p *VacancyPresenter) PresentVacancyDescriptions(vacancyDescriptions []string, vacancies []domain.Vacancy) {
	for i := 0; i < len(vacancyDescriptions); i++ {
		if i >= len(vacancies) {
			break
		}

		vacancy := vacancies[i]
		description := vacancyDescriptions[i]
		fullLink := "https://career.habr.com" + vacancy.Link

		fmt.Printf("Description %d: %s\n\nLink: %s\n\n", i+1, description, fullLink)
	}
}
