package repository

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/tebeka/selenium"
	"home/aidamur/dev/selenium/internal/domain"
	"io"
	"log"
	"net/http"
	"sync"
)

type VacancyStorager interface {
	Create(vacancy domain.Vacancy) error
	GetByID(id string) (*domain.Vacancy, error)
	GetList() ([]domain.Vacancy, error)
	Delete(id string) error
}

// VacancyRepository представляет репозиторий для работы с вакансиями
type VacancyRepository struct {
	wd        selenium.WebDriver
	vacancies map[string]domain.Vacancy
	sync.Mutex
}

// NewVacancyRepository создает новый экземпляр VacancyRepository
func NewVacancyRepository(wd selenium.WebDriver) *VacancyRepository {
	return &VacancyRepository{
		wd:        wd,
		vacancies: make(map[string]domain.Vacancy),
	}
}

// GetVacancies возвращает список вакансий на указанной странице с заданным запросом
func (r *VacancyRepository) GetVacancies(page int, query string) ([]domain.Vacancy, error) {
	err := r.wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))
	if err != nil {
		return nil, fmt.Errorf("failed to get vacancies: %v", err)
	}

	links, err := r.getVacancyLinks()
	if err != nil {
		return nil, fmt.Errorf("failed to get vacancy links: %v", err)
	}

	vacancies := make([]domain.Vacancy, len(links))
	for i, link := range links {
		description, err := r.GetVacancyDescription(link)
		if err != nil {
			log.Printf("failed to get vacancy description for link %s: %v", link, err)
			continue
		}

		vacancies[i] = domain.Vacancy{
			Title:       "",
			Description: description,
			Link:        link,
		}
	}

	return vacancies, nil
}

// getVacancyLinks возвращает ссылки на вакансии на текущей странице
func (r *VacancyRepository) getVacancyLinks() ([]string, error) {
	elements, err := r.wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
	if err != nil {
		return nil, fmt.Errorf("failed to find elements: %v", err)
	}

	var links []string
	for _, element := range elements {
		href, err := element.GetAttribute("href")
		if err != nil {
			continue
		}
		links = append(links, href)
	}

	return links, nil
}

// GetVacancyDescription возвращает описание вакансии по указанной ссылке
func (r *VacancyRepository) GetVacancyDescription(link string) (string, error) {
	resp, err := http.Get("https://career.habr.com" + link)
	if err != nil {
		return "", fmt.Errorf("failed to get vacancy page: %v", err)
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {

		}
	}(resp.Body)

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return "", fmt.Errorf("failed to parse HTML document: %v", err)
	}

	description := doc.Find("script[type=\"application/ld+json\"]").First().Text()
	return description, nil
}

func (r *VacancyRepository) GetVacancyCount(query string) (string, error) {
	elem, err := r.wd.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		return "", err
	}
	vacancyCountRaw, err := elem.Text()
	if err != nil {
		return "", err
	}
	return vacancyCountRaw, nil
}

// Create Создает вакансию
func (r *VacancyRepository) Create(vacancy *domain.Vacancy) error {
	r.Lock()
	defer r.Unlock()
	if _, exists := r.vacancies[vacancy.ID]; exists {
		return fmt.Errorf("vacancy with ID %v already exists", vacancy.ID)
	}
	r.vacancies[vacancy.ID] = *vacancy

	return nil
}

// Delete удаляет вакансию по указанному ID
func (r *VacancyRepository) Delete(id string) error {
	r.Lock()
	defer r.Unlock()
	// Проверяем, существует ли вакансия с указанным ID
	if _, exists := r.vacancies[id]; !exists {
		return fmt.Errorf("vacancy with ID %s not found", id)
	}
	delete(r.vacancies, id)
	return nil
}

// GetList возвращает список всех вакансии
func (r *VacancyRepository) GetList() ([]domain.Vacancy, error) {
	r.Lock()
	defer r.Unlock()
	vacancies := make([]domain.Vacancy, 0, len(r.vacancies))
	for _, vacancy := range r.vacancies {
		vacancies = append(vacancies, vacancy)
	}
	return vacancies, nil
}

// GetByID возвращает вакансию по указанному идентификатору
func (r *VacancyRepository) GetByID(id string) (*domain.Vacancy, error) {
	r.Lock()
	defer r.Unlock()
	// Проверяем, существует ли вакансия с указанным идентификатором
	vacancy, ok := r.vacancies[id]
	if !ok {
		return nil, fmt.Errorf("vacancy with ID %s not found", id)
	}
	return &vacancy, nil
}
