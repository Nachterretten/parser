package handlers

import (
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"home/aidamur/dev/selenium/internal/service"

	"home/aidamur/dev/selenium/internal/config"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type APIServer struct {
	config  *config.Config
	service *service.VacancyService
}

func New(config *config.Config, vacancyService *service.VacancyService) *APIServer {
	return &APIServer{
		config:  config,
		service: vacancyService,
	}
}

func (s *APIServer) Run() {
	server := &http.Server{
		Addr: s.config.HTTPAddr,
	}

	log.Printf("Listening on port %s", s.config.HTTPAddr)

	idConnClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt, syscall.SIGTERM)
		<-sigint
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := server.Shutdown(ctx); err != nil {
			log.Fatalln(err)
		}
		close(idConnClosed)
	}()

	if err := server.ListenAndServe(); err != nil {
		if !errors.Is(err, http.ErrServerClosed) {
			log.Fatalln(err)
		}
	}
	<-idConnClosed
	log.Println("Server stopped")
}

func SetupRouter(s *APIServer) *gin.Engine {
	router := gin.Default()

	// Обработчик для отображения Swagger UI
	router.GET("/swagger", s.SwaggerUI)

	// Обработчик для статических файлов в папке "/public/"
	router.GET("/public/*filepath", func(c *gin.Context) {
		c.File("./public/" + c.Param("filepath"))
	})
	router.POST("/vacancies", s.CreateHandler)
	router.DELETE("/vacancies/:id", s.DeleteHandler)
	router.GET("/vacancies", s.GetListHandler)
	router.GET("/vacancies/:id", s.GetByIDHandler)

	return router
}
