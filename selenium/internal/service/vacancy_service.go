package service

import (
	"fmt"
	_ "github.com/tebeka/selenium"
	"home/aidamur/dev/selenium/internal/domain"
	"home/aidamur/dev/selenium/internal/repository"
	"regexp"
)

// VacancyService представляет сервис для работы с вакансиями
type VacancyService struct {
	vacancyRepo *repository.VacancyRepository
}

// NewVacancyService создает новый экземпляр VacancyService
func NewVacancyService(vacancyRepo *repository.VacancyRepository) *VacancyService {
	return &VacancyService{
		vacancyRepo: vacancyRepo,
	}
}

// GetVacancies возвращает список вакансий на указанной странице с заданным запросом
func (s *VacancyService) GetVacancies(page int, query string) ([]domain.Vacancy, error) {
	vacancies, err := s.vacancyRepo.GetVacancies(page, query)
	if err != nil {
		return nil, err
	}

	for i := range vacancies {
		vacancies[i].Title = s.extractTitleFromDescription(vacancies[i].Description)
	}

	return vacancies, nil
}

// extractTitleFromDescription извлекает название вакансии из описания
func (s *VacancyService) extractTitleFromDescription(description string) string {
	// Используем регулярное выражение для извлечения названия вакансии
	re := regexp.MustCompile(`(?i)title\s*([^\n]+)`)
	matches := re.FindStringSubmatch(description)
	if len(matches) > 1 {
		return matches[1]
	}
	return ""
}

func (s *VacancyService) GetVacancyDescriptions(vacancies []domain.Vacancy) ([]string, error) {
	var vacancyDescriptions []string
	for _, vacancy := range vacancies {
		description, err := s.vacancyRepo.GetVacancyDescription(vacancy.Link)
		if err != nil {
			return nil, fmt.Errorf("failed to get vacancy description for link %s: %v", vacancy.Link, err)
		}
		vacancy.Title = s.extractTitleFromDescription(description) // Добавить вызов extractTitleFromDescription
		vacancyDescriptions = append(vacancyDescriptions, description)
	}
	return vacancyDescriptions, nil
}

func (s *VacancyService) GetVacancyCount(query string) (string, error) {
	count, err := s.vacancyRepo.GetVacancyCount(query)
	if err != nil {
		return "", fmt.Errorf("failed to get vacancy count: %v", err)
	}
	return count, nil
}

func (s *VacancyService) Create(vacancy *domain.Vacancy) error {
	err := s.vacancyRepo.Create(vacancy)
	if err != nil {
		return fmt.Errorf("error when creating: %v", err)
	}
	return nil
}

func (s *VacancyService) Delete(id string) error {
	err := s.vacancyRepo.Delete(id)
	if err != nil {
		return fmt.Errorf("error when deleting: %v", err)
	}
	return nil
}

func (s *VacancyService) GetList() ([]domain.Vacancy, error) {
	vacancies, err := s.vacancyRepo.GetList()
	if err != nil {
		return nil, fmt.Errorf("error when getting a job listing %v", err)
	}
	return vacancies, nil
}

func (s *VacancyService) GetByID(id string) (*domain.Vacancy, error) {
	vacancy, err := s.vacancyRepo.GetByID(id)
	if err != nil {
		return nil, fmt.Errorf("error when getting a job by id: %v", err)
	}
	return vacancy, nil
}
