package selenium

import (
	"home/aidamur/dev/selenium/internal/domain"
)

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

// swagger:route POST /vacancies vacancy createVacancyRequest
// Create a new vacancy.
// responses:
//
//	201: createVacancyResponse
//	400: errorResponse
//	500: errorResponse
//
// swagger:parameters createVacancyRequest
type createVacancyRequest struct {
	// Vacancy object to create
	// in: body
	Body domain.Vacancy
}

// swagger:response createVacancyResponse
type createVacancyResponse struct {
	// Created vacancy object
	// in: body
	Body domain.Vacancy
}

// swagger:route DELETE /vacancies/{id} vacancy deleteVacancyRequest
// Delete a vacancy by ID.
// responses:
//
//	200: deleteVacancyResponse
//	404: errorResponse
//
// swagger:parameters deleteVacancyRequest
type deleteVacancyRequest struct {
	// ID of the vacancy to delete
	// in: path
	ID string `json:"id"`
}

// swagger:response deleteVacancyResponse
type deleteVacancyResponse struct {
	// in: body
	Body string
}

// swagger:route GET /vacancies vacancy getListRequest
// Get the list of vacancies.
// responses:
//
//	200: getListResponse
//	500: errorResponse
//
// swagger:parameters getListRequest
type getListRequest struct {
	// No parameters
}

// swagger:response getListResponse
type getListResponse struct {
	// in: body
	Body []domain.Vacancy
}

// swagger:route GET /vacancies/{id} vacancy getByIDRequest
// Get a vacancy by ID.
// responses:
//
//	200: getByIDResponse
//	404: errorResponse
//
// swagger:parameters getByIDRequest
type getByIDRequest struct {
	// ID of the vacancy to get
	// in: path
	ID string `json:"id"`
}

// swagger:response getByIDResponse
type getByIDResponse struct {
	// in: body
	Body domain.Vacancy
}

// swagger:response errorResponse
type errorResponse struct {
	// in: body
	Body struct {
		Error string `json:"error"`
	}
}
