package main

import (
	"home/aidamur/dev/selenium/internal/config"
	"home/aidamur/dev/selenium/internal/handlers"
	"home/aidamur/dev/selenium/internal/repository"
	"home/aidamur/dev/selenium/internal/service"
	"log"
	"time"

	"github.com/tebeka/selenium"

	"home/aidamur/dev/selenium/internal/driver"
)

func main() {

	wd, err := driver.CreateWebDriver()
	if err != nil {
		log.Fatalf("Failed to create WebDriver: %v", err)
	}
	defer func(wd selenium.WebDriver) {
		err := wd.Quit()
		if err != nil {
			log.Fatalf("Невозжно подключиться к селениуму")
		}
	}(wd)

	// Создаем экземпляры репозитория, сервиса и презентера
	vacancyRepo := repository.NewVacancyRepository(wd)
	vacancyService := service.NewVacancyService(vacancyRepo)
	/*	vacancyPresenter := presentation.NewVacancyPresenter()*/

	/*	page := 1         // номер страницы
		query := "golang" // запрос

		// Получаем вакансии
		vacancies, err := vacancyService.GetVacancies(page, query)
		if err != nil {
			log.Fatalf("failed to get vacancies: %v", err)
		}

		// Выводим список вакансий
		vacancyPresenter.PresentVacancies(vacancies)

		// Получаем описания вакансий
		vacancyDescriptions, err := vacancyService.GetVacancyDescriptions(vacancies)
		if err != nil {
			log.Fatalf("failed to get vacancy descriptions: %v", err)
		}

		// Выводим описания вакансий
		vacancyPresenter.PresentVacancyDescriptions(vacancyDescriptions, vacancies)

		// Получаем кол-во вакансии
		count, err := vacancyService.GetVacancyCount(".search-total")
		if err != nil {
			log.Fatalf("failed to get vacancy count: %v", err)
		}

		// Выводим кол-во вакансии
		vacancyPresenter.PresentVacancyCount(count)*/

	// Создаем экземпляр конфигурации
	cfg := config.NewConfig()

	// Создаем экземпляр сервера API
	apiServer := handlers.New(cfg, vacancyService)

	// Запускаем сервер API
	go func() {
		router := handlers.SetupRouter(apiServer)
		err := router.Run(cfg.HTTPAddr)
		if err != nil {
			log.Fatalf("failed to start API server: %v", err)
		}
	}()
	// Ждем 150 секунд, чтобы успеть посмотреть результат
	time.Sleep(150 * time.Second)
}
